%global grilo_version 0.3.15
%global goa_version 3.17.91

Name:           grilo-plugins
Version:        0.3.16
Release:        1
Summary:        Plugins for the Grilo framework
License:        LGPLv2+
URL:		    https://wiki.gnome.org/Projects/Grilo
Source0:        https://download.gnome.org/sources/grilo-plugins/0.3/grilo-plugins-%{version}.tar.xz

BuildRequires:  gcc meson avahi-gobject-devel glib2-devel grilo-devel >= %{grilo_version}
BuildRequires:  gom-devel gperf intltool gettext gnome-online-accounts%{_isa} >= %{goa_version}
BuildRequires:  libgcrypt-devel libxml2-devel itstool libarchive-devel libmediaart-devel
BuildRequires:  libsoup3-devel lua-devel rest-devel sqlite-devel libgdata-devel
BuildRequires:  totem-pl-parser-devel tracker3-devel libdmapsharing-devel json-glib-devel cmake liboauth-devel
Requires:       gnome-online-accounts%{_isa} >= %{goa_version} grilo%{_isa} >= %{grilo_version}

%description
The framework named grilo which provides access to different sources of multimedia
content, using a pluggable system. This package`s plugins to get information from
theses sources:Apple Trailers,Bookmarks,Euronews,Filesystem,Flickr,Freebox,Gravatar,
iTunes Music Sharing,Jamendo,Last.fm,Local metadata,Metadata Store,Pocket,Podcasts,
Radio France,Shoutcast,The Guardian Videos,Tracker,Vimeo,Youtube

%prep
%autosetup -n grilo-plugins-%{version} -p1

%build
%meson -Denable-shoutcast=no -Denable-bookmarks=yes \
       -Denable-dleyna=no -Denable-dmap=yes -Denable-filesystem=yes \
       -Denable-flickr=yes -Denable-freebox=yes -Denable-gravatar=yes \
       -Denable-lua-factory=yes -Denable-metadata-store=yes \
       -Denable-podcasts=yes -Denable-tmdb=yes \
       -Denable-youtube=no -Denable-tracker=no -Denable-tracker3=yes

%meson_build

%install
%meson_install
%delete_la
%find_lang grilo-plugins --with-gnome

%files -f grilo-plugins.lang
%license COPYING
%doc AUTHORS NEWS README.md
%doc %{_datadir}/help/*/examples/example-tmdb.c
%{_datadir}/grilo-plugins/
%{_libdir}/pkgconfig/*.pc
%{_libdir}/grilo-0.3/*.so*

%changelog
* Thu Nov 23 2023 lwg <liweiganga@uniontech.com> - 0.3.16-1
- update to version 0.3.16

* Fri Sep 01 2023 xu_ping <707078654@qq.com> - 0.3.15-2
- Remove dependence on dleyna-server because it has declined.

* Mon Jan 2 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 0.3.15-1
- Update to 0.3.15

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.3.14-2
- fix build when Meson >= 0.61.5
- remove meson option enable-jamendo, enable-static, enable-vimeo

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.3.14-1
- Update to 0.3.14

* Fri Feb 25 2022 weijin deng <weijin.deng@turbolinux.com.cn> - 0.3.13-3
- Solve bug of "PANIC: unprotected error in call to Lua API
  (attempt to call a nil value) Aborted"

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.3.13-2
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Thu Jun 17 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 0.3.13-1
- Upgrade to 0.3.13
- Change meson configurations

* Mon Jun 7 2021 baizhonggui <baizhonggui@huawei.com> - 0.3.8-4
- Fix building error: /usr/bin/git: No such file or directory
- Add git/cmake/liboauth-devel in BuildRequires


* Thu Jul 16 2020 lingsheng <lingsheng@huawei.com> - 0.3.8-3
- Add buildrequire avahi-gobject to fix build fail

* Thu Apr 16 2020 Jeffery.Gao <gaojianxing@huawei.com> - 0.3.8-2
- Package init
